PROMPT='%{$fg_bold[red]%}%n%{$reset_color%} at %{$fg_bold[cyan]%}%m%{$reset_color%} in %{$fg[green]%}$(collapse_pwd)%{$reset_color%} %{$fg[yellow]%}$(prompt_char)%{$reset_color%} $(git_prompt_info) '
RPROMPT='$(battery_charge)'

function collapse_pwd {
    echo $(pwd | sed -e "s,^$HOME,~,")
}


export BAT_CHARGE='/home/pedro/.bin/script_bat.py'
function battery_charge {
    echo `$BAT_CHARGE` 2>/dev/null
}


function prompt_char {
    git branch >/dev/null 2>/dev/null && echo '±' && return
    hg root >/dev/null 2>/dev/null && echo '☿' && return
    echo '→'
}

ZSH_THEME_GIT_PROMPT_PREFIX="on %{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[green]%}!"
ZSH_THEME_GIT_PROMPT_CLEAN=""
